<?php

/**
 * Настройки платежной системы Wallet One для административного интерфейса
 * 
 * @package    DIAFAN.CMS
 * @author     Wallet One
 * @version    3.0.0
 * @license    
 * @copyright  Copyright (c) 2015 Wallet One (http://www.walletone.com)
 * This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
if (!defined('DIAFAN')) {
  $path = __FILE__;
  $i = 0;
  while (!file_exists($path . '/includes/404.php')) {
    if ($i == 10)
      exit;
    $i++;
    $path = dirname($path);
  }
  include $path . '/includes/404.php';
}

class Payment_W1_admin {
  
  public $config;
  
  private $diafan;
  
  /**
   * Real version CMS 
   * 
   * @var string 
   */
  public $version;
  
  /**
   * Declaration of class client
   * 
   * @var object
   */
  public $client;
  
  /**
   * Object of w1PaymentGateway class.
   * 
   * @var type 
   */
  public $logger;
  
  /**
   * Absolute path to direction of module 
   * 
   * @var string
   */
  public $homeDir;

  public function __construct(&$diafan) {
    $this->diafan = &$diafan;
    
    $parametrs = DB::query_fetch_array("SELECT `params` FROM {payment} WHERE payment='w1'");
    $param = unserialize($parametrs['params']);
    
    $this->version = VERSION_CMS;
    if($update['created'] > strtotime('01-01-2016')) {
      $this->version = 6;
    }
    
    include_once 'walletone/Classes/W1Client.php';
    $this->client = \WalletOne\Classes\W1Client::init()->run($lang, 'diafan');
    
    $this->logger = \Logger::getLogger(__CLASS__);
    
    $this->homeDir = '/modules/payment/backend/w1/';
    
    $update = DB::query_fetch_array("SELECT created FROM {update_return} WHERE `current` = '1'");
    
    $status_list = [];
    $status = DB::query_fetch_all("SELECT `id`, `name1` FROM {shop_order_status} WHERE `trash` = '0'");
    foreach ($status as $value) {
      $status_list[$value['id']] = $value['name1'];
    }
    
    $this->config = array(
      "name" => w1Title,
      "params" => array(
        'icon' => array(
          'type' => 'function',
          'name' => w1SettingsLogo
        ),
        'generate' => array(
          'type' => 'checkbox',
          'name' => w1SettingsCreateIcon,
          'help' => w1SettingsCreateIcon,
          'default' => true,
        ),
        'MERCHANT_ID' => array(
          'type' => 'text',
          'name' => w1SettingsMerchant.' *',
          'help' => w1SettingsMerchantDesc
        ),
        'SIGNATURE_METHOD' => array(
          'type' => 'function',
          'name' => w1SettingsSignatureMethod.' *',
          'help' => w1SettingsSignatureMethodDesc,
          'value' => $this->client->getSettings()->signatureMethodArray,
          'default' => 'md5'
        ),
        'SIGNATURE' => array(
          'type' => 'text',
          'name' => w1SettingsSignature.' *',
          'help' => w1SettingsSignatureDesc
        ),
        'CURRENCY_ID' => array(
          'type' => 'function',
          'name' => w1SettingsCurrency.' *',
          'value' => $this->client->getSettings()->currencyName,
          'default' => 643
        ),
        'currency_default' => array(
          'type' => 'checkbox',
          'name' => w1SettingsCurrencyDefault,
          'help' => w1SettingsCurrencyDefaultDesc,
          'default' => false
        ),
        'CULTURE_ID' => array(
          'type' => 'function',
          'name' => w1SettingsCulture.' *',
          'value' => $this->client->getSettings()->cultureArray,
          'default' => 'ru-RU'
        ),
        'order_status_sucess' => array(
          'type' => 'function',
          'name' => w1SettingsOrderStatusSuccess,
          'value' => $status_list,
          'default' => '2'
        ),
        'order_status_waiting' => array(
          'type' => 'function',
          'name' => w1SettingsOrderStatusWaiting,
          'value' => $status_list,
          'default' => '1'
        ),
        'return_url' => array(
          'type' => 'function',
          'name' => w1SettingsReturnUrl,
          'help' => w1SettingsReturnUrlDesc,
          'default' => BASE_PATH . 'payment/get/w1/result/'
        ),
        'PTENABLED' => array(
          'type' => 'function',
          'name' => 'Разрешенные платежные системы'
        ),
        'PTDISABLED' => array(
          'type' => 'function',
          'name' => 'Запрещенные платежные системы'
        )
      )
    );
    
  }
  
  /**
   * Editing a field "Signature generation method"
   *
   * @return void
   */
  public function edit_variable_SIGNATURE_METHOD($value) {
    $html = $this->client->getHtml()->generateSelect(
        $this->config['params']['SIGNATURE_METHOD']['name'],
        $this->config['params']['SIGNATURE_METHOD']['help'],
        'w1_SIGNATURE_METHOD',
        $this->config['params']['SIGNATURE_METHOD']['value'],
        $this->config['params']['SIGNATURE_METHOD']['default'],
        $value,
        $this->version
    );
    echo $html;
  }

  /**
   * Editing a field "A currency identifier by default"
   *
   * @return void
   */
  public function edit_variable_CURRENCY_ID($value) {
    $html = $this->client->getHtml()->generateSelect(
        $this->config['params']['CURRENCY_ID']['name'],
        w1SettingsReturnUrlDesc, 'w1_CURRENCY_ID',
        $this->config['params']['CURRENCY_ID']['value'],
        $this->config['params']['CURRENCY_ID']['default'],
        $value,
        $this->version
    );
    echo $html;
  }
  
  /**
   * Editing a field "A currency identifier by default"
   *
   * @return void
   */
  public function edit_variable_CULTURE_ID($value) {
    $html = $this->client->getHtml()->generateSelect(
        $this->config['params']['CULTURE_ID']['name'],
        w1SettingsReturnUrlDesc, 'w1_CULTURE_ID',
        $this->config['params']['CULTURE_ID']['value'],
        $this->config['params']['CULTURE_ID']['default'],
        $value,
        $this->version
    );
    echo $html;
  }
  
  /**
   * Editing a field "A currency identifier by default"
   *
   * @return void
   */
  public function edit_variable_order_status_sucess($value) {
    $html = $this->client->getHtml()->generateSelect(
        $this->config['params']['order_status_sucess']['name'],
        '',
        'w1_order_status_sucess',
        $this->config['params']['order_status_sucess']['value'],
        $this->config['params']['order_status_sucess']['default'],
        $value,
        $this->version
    );
    echo $html;
  }
  
  /**
   * Editing a field "A currency identifier by default"
   *
   * @return void
   */
  public function edit_variable_order_status_waiting($value) {
    $html = $this->client->getHtml()->generateSelect(
        $this->config['params']['order_status_waiting']['name'],
        '',
        'w1_order_status_waiting',
        $this->config['params']['order_status_waiting']['value'],
        $this->config['params']['order_status_waiting']['default'],
        $value,
        $this->version
    );
    echo $html;
  }
  
  /**
   * Editing a field "A currency identifier by default"
   *
   * @return void
   */
  public function edit_variable_order_status_fail($value) {
    $html = $this->client->getHtml()->generateSelect(
        $this->config['params']['order_status_fail']['name'],
        '',
        'w1_order_status_fail',
        $this->config['params']['order_status_fail']['value'],
        $this->config['params']['order_status_fail']['default'],
        $value,
        $this->version
    );
    echo $html;
  }
  
  /**
   * Editing a field "Link for URL"
   *
   * @return void
   */
  public function edit_variable_return_url($value) {
    $html = $this->client->getHtml()->generateInputDisabled($this->config['params']['return_url'], $this->version, $this->homeDir);
    echo $html;
  }

  /**
   * Editing a field "icon"
   * @return void
   */
  public function edit_variable_icon() {
    $parametrs = DB::query_fetch_array("SELECT `params` FROM {payment} WHERE payment='w1'");
    $param = unserialize($parametrs['params']);
    $html = $this->client->getHtml()->generateIcons($this->config['params']['icon'], $this->version, $param['icon']);
    echo $html;
  }

  /**
   * Saving field "icon"
   * @return void
   */
  public function save_variable_icon() {
    if (!empty($_FILES['w1_icon']) && $_FILES['w1_icon']['error'] == 0 && is_uploaded_file($_FILES['w1_icon']['tmp_name']) == true && strpos($_FILES['w1_icon']['type'], 'image') !== false) {
      $str = explode('.', $_FILES['w1_icon']['name']);
      $type_file = $str[count($str) - 1];
      $path = $this->homeDir;
      $uploadfile = str_replace('\\', '//', __DIR__) . '/logo.' . $type_file;
      $files = array_diff(scandir(__DIR__), array('..', '.'));
      foreach ($files as $value) {
        if (strpos($value, 'logo') !== false) {
          unlink(__DIR__ . '/' . $value);
        }
      }
      if (move_uploaded_file($_FILES['w1_icon']['tmp_name'], $uploadfile) == true) {
        $logo = $path . 'logo.' . $type_file;
        return $_POST['w1_icon'] = $logo;
      }
      else{
        $this->logger->warn('Not save icon');
      }
    }
    elseif(!empty($_POST['w1_icon'])){
      return $_POST['w1_icon'];
    }
    else{
      return false;
    }
  }

  /**
   * Editing a field "Permitted payment systems"
   * @return void
   */
  public function edit_variable_PTENABLED($value) {
    $this->method_payments('PTENABLED', $value);
  }

  /**
   * Editing a field "Disabled payment systems"
   * @return void
   */
  public function edit_variable_PTDISABLED($value) {
    $this->method_payments('PTDISABLED', $value);
  }

  public function method_payments($name, $value) {
    $html = $this->client->getHtmlPayments($name, $value);
    
    if(intval($this->version) == 5) {
      echo '<tr class="tr_payment" payment="w1" style="display:none">
                    <td class="td_first">
                        <label">' . $this->config['params'][$name]['name'] . '</label>
                    </td>
                    <td>
                    '. $html . '
                      </td>';
      
    }
    else {
      echo '<div class="unit tr_payment" payment="w1" style="display:none">
              <div class="infofield">'.$this->config['params'][$name]['name'].'</div>
              <input type="hidden" name="w1_' . $name . '" value="">
              ' . $html . '</div>';
    }
  }

  /**
   * Saving field "Permitted payment systems"
   * @return void
   */
  public function save_variable_PTENABLED() {
    if (!empty($_POST['w1_generate'])) {
      $mas_ptenabled = [];
      if(!empty($_POST['w1_PTENABLED'])){
        $mas_ptenabled = $_POST['w1_PTENABLED'];
      }
      $mas_ptdisabled = [];
      if(!empty($_POST['w1_PTDISABLED'])){
        $mas_ptdisabled = $_POST['w1_PTDISABLED'];
      }
      if ($pic = $this->client->createNewIcon($mas_ptenabled, $mas_ptdisabled)) {
        if (strpos($_POST['text'], '<img') === false) {
          $_POST['text'] .= ' <img src="' . $this->homeDir.'walletone/img/'.$pic . '">';
        }
        else{
          $_POST['text'] = preg_replace('/="(.*?)"/ui', '="'.$this->homeDir.'walletone/img/'.$pic.'"', $_POST['text']);
        }
      }
    }
    return $_POST['w1_PTENABLED'];
  }
  
}
