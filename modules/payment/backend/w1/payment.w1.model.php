<?php

if (!defined('DIAFAN')) {
  $path = __FILE__;
  $i = 0;
  while (!file_exists($path . '/includes/404.php')) {
    if ($i == 10)
      exit;
    $i++;
    $path = dirname($path);
  }
  include $path . '/includes/404.php';
}

class Payment_W1_model extends Diafan {
  
  /**
   * Declaration of class client
   * 
   * @var object
   */
  public $client;
  
  /**
   * Object of w1PaymentGateway class.
   * 
   * @var type 
   */
  public $logger;
  
  public function __construct(&$diafan) {
    parent::__construct($diafan);
    
    include_once 'walletone/Classes/W1Client.php';
    $this->client = \WalletOne\Classes\W1Client::init()->run($lang, 'diafan');
    
    $this->logger = \Logger::getLogger(__CLASS__);
  }

  /**
   * Формирует данные для формы платежной системы
   * 
   * @param array $params настройки платежной системы
   * @param array $pay данные о платеже
   * @return array
   */
  public function get($params, $pay) {
    $settings = [];
    $settings['merchantId'] = $params['MERCHANT_ID'];
    $settings['signature'] = $params['SIGNATURE'];
    $settings['signatureMethod'] = $params['SIGNATURE_METHOD'];
    $settings['currencyId'] = $params['CURRENCY_ID'];
    $settings['currencyDefault'] = $params['currency_default'];
    $settings['orderStatusSuccess'] = $params['order_status_sucess'];
    $settings['orderStatusWaiting'] = $params['order_status_waiting'];
    $settings['cultureId'] = $params['CULTURE_ID'];
    $settings['orderId'] = $pay['id'];
    $settings['summ'] = number_format($pay['summ'], 2, '.', '');
    $settings['order_currency'] = 'ru';
    $settings['successUrl'] = BASE_PATH . 'payment/get/w1/success/';
    $settings['successUrl'] = BASE_PATH . 'payment/get/w1/result/?orderId='.$pay['id'];
    $settings['failUrl'] = BASE_PATH . 'payment/get/w1/result/?orderId='.$pay['id'];
    $settings['siteName'] = (!empty(TIT1) ? TIT1 : w1SiteName);
    $settings['nameCms'] = '_diafan';
    if (!empty($params['PTENABLED'])) {
      $settings['paymentSystemEnabled'] = $params['PTENABLED'];
    }
    if (!empty($params['PTDISABLED'])) {
      $settings['paymentSystemDisabled'] = $params['PTDISABLED'];
    }
    if(!empty($pay['details']['name'])){
      $settings['firstNameBuyer'] = $pay['details']['name'];
    }
    if(!empty($pay['details']['email'])){
      $settings['emailBuyer'] = $pay['details']['email'];
    }
    $settings['orderIdReal'] = $pay['element_id'];
    
    if ($this->client->validateParams($settings) !== true) {
      echo $this->client->getMessage($this->client->errors, $this->client->messages, 'html');
      return;
    }

    $fields = $this->client->createFieldsForForm();
    $result['formHtml'] = $this->client->createHtmlForm($fields);
    $result['text'] = $pay['text'];

    return $result;
  }

}
