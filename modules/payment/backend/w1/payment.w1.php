<?php

if (!defined('DIAFAN')) {
  $path = __FILE__;
  $i = 0;
  while (!file_exists($path . '/includes/404.php')) {
    if ($i == 10)
      exit;
    $i++;
    $path = dirname($path);
  }
  include $path . '/includes/404.php';
}

if(!empty($_GET['orderId']) && is_numeric($_GET['orderId'])){
  $pay = $this->diafan->_payment->check_pay(str_replace('_'.$_SERVER['HTTP_HOST'], '', (int)$_GET['orderId']), 'w1');
}
elseif (!empty ($_POST)) {
  $pay = $this->diafan->_payment->check_pay(str_replace('_'.$_SERVER['HTTP_HOST'], '', $_POST['WMI_PAYMENT_NO']), 'w1');
}

if ($_GET["rewrite"] == "w1/result") {
  include_once 'walletone/Classes/W1Client.php';
  $client = \WalletOne\Classes\W1Client::init()->run($lang, 'diafan');
    
  $logger = \Logger::getLogger(__CLASS__);
  
  File::save_file(serialize($_POST), 'tmp/w1' . time() . '_' . rand(0, 999));
  
  if(empty($_POST)){
    $order_rew = DB::query_result("SELECT rewrite FROM {rewrite} WHERE module_name='site' AND trash='0' AND "
        . "element_type='element' AND element_id IN (SELECT id FROM {site} WHERE module_name='%s' AND [act]='1' AND trash='0')", $pay['module_name']);
    $text = sprintf(w1OrderResultWaitingPayment, $pay["element_id"], $_POST['WMI_ORDER_STATE'], $_POST['WMI_ORDER_ID']);
    DB::query("UPDATE {shop_order} SET status_id='%s', status=status+1 WHERE id=%d", $pay['params']['order_status_waiting'], $pay["element_id"]);
    $logger->info($text);
    DB::query("UPDATE {payment_history} SET status='pay', created=%d WHERE id=%d", time(), $pay["id"]);
		$this->diafan->redirect('http'.(IS_HTTPS ? "s" : '').'://'.getenv("HTTP_HOST").'/'.(REVATIVE_PATH ? REVATIVE_PATH.'/' : '').$order_rew.'/step3/');
    
  }
  $settings = [];
  $settings['merchantId'] = $pay['params']['MERCHANT_ID'];
  $settings['signature'] = $pay['params']['SIGNATURE'];
  $settings['signatureMethod'] = $pay['params']['SIGNATURE_METHOD'];
  $settings['currencyId'] = $pay['params']['CURRENCY_ID'];
  $settings['currencyDefault'] = $pay['params']['currency_default'];
  $settings['orderStatusSuccess'] = $pay['params']['order_status_sucess'];
  $settings['orderStatusWaiting'] = $pay['params']['order_status_waiting'];
  $settings['orderPaymentId'] = $_POST['WMI_ORDER_ID'];
  $settings['orderState'] = mb_strtolower($_POST['WMI_ORDER_STATE']);
  $settings['orderId'] = str_replace('_' . $_SERVER['HTTP_HOST'], '', $_POST['WMI_PAYMENT_NO']);
  $settings['paymentType'] = $_POST['WMI_PAYMENT_TYPE'];
  $settings['summ'] = $_POST['WMI_PAYMENT_AMOUNT'];
  if ($client->resultValidation($settings, $_POST)) {
    $result = $client->getResult();
    //check for the existence of the number order
    if ($pay['id'] != $result->orderId) {
      $error = sprintf(w1ErrorResultOrder, $pay["element_id"], $result->orderState, $result->orderPaymentId);
      $logger->info($error);
      ob_start();
      echo 'WMI_RESULT=RETRY&WMI_DESCRIPTION=' . $error;
      die();
    }
    //checking on the order amount
    if (number_format($pay['summ'], 2, '.', '') != $result->summ) {
      $error = sprintf(w1ErrorResultOrderSumm, $result->orderId, $result->orderState, $result->orderPaymentId);
      $logger->info($error);
      ob_start();
      echo 'WMI_RESULT=RETRY&WMI_DESCRIPTION=' . $error;
      die();
    }
    if ($pay['status'] != $settings['orderStatusSuccess'] && $result->orderState == 'accepted') {
      $text = sprintf(w1OrderResultSuccess, $pay["element_id"], $result->orderState, $result->orderPaymentId);
      DB::query("UPDATE {shop_order} SET status_id='%s', status=status+1 WHERE id=%d", $settings['orderStatusSuccess'], $pay["element_id"]);
      $logger->info($text);
      DB::query("UPDATE {payment_history} SET status='pay', created=%d WHERE id=%d", time(), $pay["id"]);
      ob_start();
      echo 'WMI_RESULT=OK';
      die();
    }
    $order_rew = DB::query_result("SELECT rewrite FROM {rewrite} WHERE module_name='site' AND trash='0' AND element_type='element' AND element_id IN (SELECT id FROM {site} WHERE module_name='%s' AND [act]='1' AND trash='0')", $pay['module_name']);
    $this->diafan->redirect('http'.(IS_HTTPS ? "s" : '').'://'.getenv("HTTP_HOST").'/'.(REVATIVE_PATH ? REVATIVE_PATH.'/' : '').$order_rew.'/step3/');
  }
  else {
    if (!empty($client->errors)) {
      $logger->info('13131313131');
      ob_start();
      echo 'WMI_RESULT=RETRY&WMI_DESCRIPTION=' . implode(' ', $client->errors);
      die();
    }
  }
  ob_start();
  echo 'WMI_RESULT=RETRY&WMI_DESCRIPTION=ERROR';
  $logger->warn('Other error');
  die();
}

// оплата прошла успешно
if ($_GET["rewrite"] == "w1/success") {
  $this->diafan->_payment->success($pay, 'redirect');
}

$this->diafan->_payment->fail($pay);