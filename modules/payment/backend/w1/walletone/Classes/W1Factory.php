<?php

namespace WalletOne\Classes;

class W1Factory{
  
  public static function createHtmlClass($config, $className, $lang = 'ru'){
    $object = null;
    switch ($className) {
      case 'diafan':
        $object = new \WalletOne\Classes\ExtraMethods\W1HtmlDiafan($config, $lang);
        break;
      default:
        $object = new \WalletOne\Classes\StandardsMethods\W1Html($config, $lang);
        break;
    }
    
    return $object;
  }
  
  public static function createPaymentsClass($config, $className, $lang = 'ru'){
    $object = null;
    switch ($className) {
      case 'diafan':
        $object = new \WalletOne\Classes\ExtraMethods\W1PaymentsDiafan($config, $lang);
        break;
      default:
        $object = new \WalletOne\Classes\StandardsMethods\W1Payments($config, $lang);
        break;
    }
    
    return $object;
  }
}

