# README #
This module is designed to receive payment orders for CMS DIAFAN

=== Wallet One Payment ===
Contributors: h-elena
Tags: Wallet One, DIAFAN, buy now, payment, payment for diafan, wallet one integration, Wallet One payment
Version: 3.1.0
Requires at least: 5.4
Tested up to: 6.0.4.3
Stable tag: 6.0.4.3
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.en.html

The Wallet One module is a payment system for CMS DIAFAN. He it allows to pay for your orders on the site.

== Description ==
If you have an online store on CMS DIAFAN, then you need a module payments for orders made. This will help you plug the payment system Wallet One. With our system you will be able to significantly expand the method of receiving payment. This will lead to an increase in the number of customers to your store.

== Installation ==
1. Register on the site http://www.walletone.com
2. Download the module files on the site.
3. Instructions for configuring the plugin is in the file read.pdf.

== Screenshots ==
Screenshots are to be installed in the file read.pdf

== Changelog ==
= 1.1.1 =
* Fix - обновлеине результата обработки на случай оплаты не онлайн

= 1.1.2 =
* Fix - Фиксация бага по ответу платежной системы

= 1.1.3 =
* Fix - Фиксация бага по ответу платежной системе и замена иконок

= 2.0.0 =
* Added new universal classes and new settings

= 2.0.1 =
* Fix - fixed bug with undefined variables

= 2.0.2 =
* Fix - fixed bug with return back from payment system with rejected status

= 2.0.3 =
* Fix - fixed bug with checkin signature in codding string windows-1251

= 2.0.4 =
* Fix - fixed bug with e-mail user

= 3.0.0 =
* Fix - fixed bug with anser from calback payment system

= 3.1.0 =
* Change expired date for invoice(30 days)

== Frequently Asked Questions ==
No recent asked questions 

== Upgrade Notice ==
No recent asked updates
